# 2048

I had some time to kill while sitting on airplanes during my summer vacation.
I had been playing 2048 on my phone.
It's a fun game; think a cross between tetris and sudoku.
I thought it would be fun to recreate it in Forth.
It seemed a good opportunity to excercise the Forth concepts I have learned so far.

At the time of this writing, it's playable,
but incomplete.
For example, if you fill the grid,
`new` will get stuck in an infinite loop.
Also, the game doesn't know whether you've won or lost.
Also, the game will place a new tile even if you make an invalid move.

So, there are a number of things that need to be finished.
I've had fun writing this so far,
but I need to move on for now.

[![asciicast](https://asciinema.org/a/9rSioiFnpBDtFWqmDVZyIWuxg.svg)](https://asciinema.org/a/9rSioiFnpBDtFWqmDVZyIWuxg)
